
# OpenSim models 

A collection of different musculoskeletal models to be used with OpenSim software.


## How to use
If you already have an openSim installation you just have to download the repository to your PC and you can use them directly. You can also clone the repo using:
```
git clone git@gitlab.inria.fr:human/opensim_models.git
```

> If you want to use only one model and copy it somewhere, just make sure to copy the Geometry file with it as well.

## Model list
```bash
.
├── full_body
│   └── gait2392_simbody.osim
├── lower_body=
│   └── leg6dof9musc.osim
└── upper_body
    ├── bimanual
    │   ├── full_upper_body_marks.osim
    │   └── MoBL_ARMS_bimanual_6_2_21.osim
    └── unimanual
        ├── arm26.osim
        ├── arm310.osim
        ├── arm38.osim
        ├── arm720.osim
        ├── OSarm412.osim
        ├── Wu_Shoulder_Model.osim
        |
        ├── Holzbaur-Stanford-UpperExtremityModel
        |   ├── Holzbaur-adapted.osim
        |   └── Stanford VA upper limb model_0.osim
        | 
        └── MoBL-ARMS Upper Extremity Model
            ├── MOBL_ARMS_fixed_33.osim
            ├── MOBL_ARMS_fixed_41.osim
            ├── Papers
            │   ├── Holzbaur et al. 2005.pdf
            │   ├── McFarland et al. 2019.pdf
            │   └── Saul et al. 2015.pdf
            └── Readme.txt
```
